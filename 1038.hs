import Text.Printf

data Item = Item
    { code :: Int
    , name :: String
    , cost :: Double
    } deriving Show;

menu =
    [ Item 1 "Cachorro Quente" 4.00
    , Item 2 "X-Salada" 4.50
    , Item 3 "X-Bacon" 5.00
    , Item 4 "Torrada Simples" 2.00
    , Item 5 "Refrigerante" 1.50]

main = do
    line <- getLine
    let (item':amount':_) = words line
    let item = read item' :: Int
    let amount = fromIntegral (read amount' :: Int)
    let value = (cost (menu !! (item-1))) * amount
    putStrLn $ printf "Total: R$ %.2f" value
