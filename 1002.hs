import Text.Printf

main = do
    r' <- getLine
    let r = read r' :: Double
    putStrLn $ printf "A=%.4f" (3.14159 * r*r)
