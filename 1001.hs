main = do
    a' <- getLine
    b' <- getLine
    let a = read a' :: Int
    let b = read b' :: Int
    putStrLn $ "X = " ++ show (a + b)
