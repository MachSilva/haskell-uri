import Data.Char

sticks :: [Int]
sticks = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6]

leds :: [Char] -> Int
leds line = sum $ map (\c -> sticks !! ((ord c) - (ord '0'))) line

test :: IO ()
test = do
    line <- getLine
    putStr $ show $ leds line
    putStrLn " leds"

main :: IO ()
main = do
    n' <- getLine
    let n = read n' :: Int
    mapM_ (const test) [1..n]
