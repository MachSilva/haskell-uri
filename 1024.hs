import Data.Char

splitInTwo :: [a] -> ([a], [a])
splitInTwo list =
    let n = length list
    in splitAt (quot n 2) list

shift :: Int -> Char -> Char
shift n letter = chr $ (ord letter) + n

pass1 :: String -> String
pass1 = map (\a -> if isLetter a then shift 3 a else a)

pass3 :: String -> String
pass3 text =
    let (a,b) = splitInTwo text
    in a ++ (map (shift (-1)) b)

encrypt :: String -> String
encrypt = pass3 . reverse . pass1

encryptLine :: IO ()
encryptLine = do
    line <- getLine
    putStrLn $ encrypt line

main :: IO ()
main = do
    n' <- getLine
    let n = read n' :: Int
    mapM_ (const encryptLine) [1..n]
