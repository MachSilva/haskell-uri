import Data.List

main = do
    line <- getLine
    let list = map (read :: String -> Int) $ words line
    mapM print $ sort list
    putStrLn ""
    mapM print list
